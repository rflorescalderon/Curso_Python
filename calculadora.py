#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 10:48:39 2018

@author: rafaelflores
"""

import operaciones as op

nom=0
while nom!= 'y':
    print('CALCULADORA')
    print('1. Suma')
    print('2. Resta')
    print('3. Multiplicación')
    print('4. División')
    a=float(input('Valor 1: '))
    b=float(input('Valor 2: '))
    cadena= str(input('Nombre de Operación: '))
    print(op.calculadora(a,b,cadena))
    nom=input('5. Salir y/n: ')