import numpy as np 
import matplotlib.pyplot as plt 

a00,a01,a10,a11=3.0,2.0,2.0,6.0
b0,b1=2.0,-8.0
A = np.array([[a00,a01],[a10,a11]])
B=np.array([b0,b1])
print('A = ',A,'\n','B = ',B)
sol=np.linalg.solve(A,B)
print(sol)

x=np.linspace(-3,3,50)
y0=-a00/a01*x+b0/a01
y1=-a01/a11*x+b1/a11

plt.plot(x,y0,label='$y_0$')
plt.plot(x,y1,label='$y_1$')
plt.plot(sol[0],sol[1],'bo')
plt.legend()
plt.grid()
plt.show()


