import numpy as np 
import matplotlib.pyplot as plt 

def f(x,y):
	return  np.sin(x+y)

L = 1
N = 6
h =L/(N+1)

x=np.linspace(0,L,N)
y=np.linspace(0,L,N)

V0 = np.random.rand((N-2)**2)

R = -4.0*np.eye(N-2)

for i in range(0,N-2):
	if i != 0:
		R[i,i-1] = 1
	if i != N-3:
		R[i,i+1] = 1

A = np.zeros(((N-2)**2,(N-2)**2))
A[ 1*(N-2)+1 : 1*(N-2)+N-1 , 1*(N-2)+1 : 1*(N-2)+N-1 ]
for i in range(0,N-2):
    if i != 0:
        A[i*(N-2)+1-1:i*(N-2)+N-1-1, i*(N-2)+1-1:i*(N-2)+N-1-1] = R 
        A[i*(N-2)+1-N+2-1:i*(N-2)+N-1-N+2-1,i*(N-2)+1-1:i*(N-2)+N-1-1] = np.eye(N-2)
        if i!=N-3 :
            print('paso')
            A[i*(N-2)+N-2:i*(N-2)+N-2+N-2, i*(N-2):i*(N-2)+N-1-1]=np.eye(N-2)

    else:
        A[0:N-2,0:N-2]=R
        A[N-2:2*N-4,0:N-2]=np.eye(N-2)
        
print(A)
print (np.linalg.det(A))
B=np.empty(((N-2)**2))
C = -1*np.ones((N-2)**2)
for i in range(N-2):
    if i !=0:
        B[i*(N-2):i*(N-2)+1+N-3] = f(x[i+1],y[1:N-1])
    elif i==0:
        B[0:N-2]=f(x[0],y[0:N-2])
B=B*h**2
C=C*h**2

W=np.linalg.solve(A,C).reshape(N-2,N-2).T
print(C)

U=W.copy()
for j in range(0,N-2):
    for i in range(0,N-2):
           W[i,j]=U[N-3-i,j]

V=np.zeros((N,N))
V[ 1 : N-1 , 1 : N-1]=W
X,Y = np.meshgrid(x,y)
print(V)
plt.contour(X,Y,V)
plt.show()


  







