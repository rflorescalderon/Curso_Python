#Álgebra numeros complejos

def suma(z1,z2):
	print((z1.real+z2.real)+(z1.imag+z2.imag)*1j)
	return (z1.real+z2.real)+(z1.imag+z2.imag)*1j
def mult(z1,z2):
	print((z1.real * z2.real - z1.imag * z2.imag) + (z2.real * z1.imag + z1.real * z2.imag) * 1j)
	return (z1.real * z2.real - z1.imag * z2.imag) + (z2.real * z1.imag + z1.real * z2.imag) * 1j
def conj(z1):
	print(z1.real - z1.imag*1j)
	return z1.real - z1.imag*1j
def inv(z):
	print((z.real - z.imag * 1j) / (z.real**2 + z.imag**2))
	return (z.real - z.imag * 1j) / (z.real**2 + z.imag**2)

suma(2j,7+3j)
inv(2j)



