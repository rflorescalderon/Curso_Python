import numpy as np 
import matplotlib.pyplot as plt 

a00,a01,a10,a11=3.0,2.0,2.0,6.0
b0,b1=2.0,-8.0
A = np.array([[a00,a01],[a10,a11]])
B=np.array([b0,b1])
print('A = ',A,'\n','B = ',B)
sol=np.linalg.solve(A,B)
print(sol)

x0=np.random.rand(2)
x=x0
for k in range(100):
	x = (B-A@x+np.diag(A)*x)/np.diag(A)
print(x)
