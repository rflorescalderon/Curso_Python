#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 10:50:20 2018

@author: rafaelflores
"""

def calculadora(a,b,cadena):
    if cadena=='Suma':
        return a+b
    elif cadena=='Multiplicación':
        return a*b
    elif cadena=='División':
        return a/b
    else :
        return a-b
        