
#NÚMEROS FELICES
print('Números felices\n')

while True:

	try:
		p=int(input('Introduce rango (entero positivo) de números para investigar:\n'))
		if p<=0:
			print("ERROR  Introduce un rango entero positivo \n")
			continue
		i,m,t=0,0,1000
		l=[]

		for n in range(p):
			r,i,m=n,0,0

			while i<t :
				m=0
				for j in range(len(str(n))):
					m += int(str(n)[j])**2	 
				if m==1:
					l.append(r)  
					break
				else:
					n = m
					i += 1
		print('Los primeros números felices dentro del rango 0,',p,' son:\n',l ,'\n')
		break
	except:
		print("ERROR  Introduce un rango entero positivo \n")


